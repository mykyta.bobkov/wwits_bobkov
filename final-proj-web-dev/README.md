![logo](img/Screenshot_1.png)
# [WWITS] Web applications developing final project

This is the final project in Wildau Welcome IT School. Author - Mykyta Bobkov

## Introduction

In this project I tried to create web page with my portfolio and experience.


## How to install & run
Firstly clone a repository:
```bash
git clone https://gitlab.com/mykyta.bobkov/wwits_bobkov.git
```
In the project directory, you can run:
Install all packages:
```bash
npm install
```


### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
