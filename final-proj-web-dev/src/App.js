import React, { useState, useEffect } from 'react'
import { FaAngleDoubleRight } from 'react-icons/fa'

function App() {
  
  const [jobs,setJobs] = useState([
    {
     id: "recAGJfiU4CeaV0HL",
     order: 3,
     title: "Student",
     dates: "2022-2025",
     duties: ["Studiyng in Faculty of Automation and computer-integrated technologies"],
     company: "Kharkiv National University of Radioelectronic"},
     {
      id: "recIL6mJNfWObonls",
     order: 2,
     title: "Participant of Wildau Welcome IT School",
     dates: "09 May 2022 - 18 August 2022",
     duties: ["Participant of the special course for ukrainian refugees created by Alina Nechyporenko, Marcus Frohme & others"],
     company: "TH Wildau"
     },
     {
      id: "rec61x18GVY99hQq5",
     order: 1,
     title: "Good spesialist in a future!",
     dates: "2022 and next",
     duties: [""],
     company: "Best company ever"
     }
    ])
  const [value, setValue] = useState(0)

  const nextTab = () => {
    if (value != jobs.length - 1) {
        setValue(value + 1);
    } else {
        setValue(0);
    }
 };
  const { company, dates, duties, title } = jobs[value]
  return (
    <section className="section">
      <div className="title">
        <h2>experience</h2>
        <div className="underline"></div>
      </div>
      <div className="jobs-center">
        {/* btn container */}
        <div className="btn-container">
          {jobs.map((item, index) => {
            return (
              <button
                key={item.id}
                onClick={() => setValue(index)}
                className={`job-btn ${index === value && 'active-btn'}`}
              >
                {item.company}
              </button>
            )
          })}
        </div>
        {/* job info */}
        <article className="job-info">
          <h3>{title}</h3>
          <h4>{company}</h4>
          <p className="job-date">{dates}</p>
          {duties.map((duty, index) => {
            return (
              <div key={index} className="job-desc">
                <FaAngleDoubleRight className="job-icon"></FaAngleDoubleRight>
                <p>{duty}</p>
              </div>
            )
          })}
        </article>
      </div>
      <button 
       onClick={nextTab}
       type="button"
       className="btn">
        next
       
      </button>
    </section>
  )
}

export default App